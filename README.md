# gitea-dark-blue theme

The dark blue Gitea theme used in https://gitea.artixlinux.org

<p align="center">
	<a href="./screenshot.png"><img src="./screenshot.png" height="400px" alt="Screenshot of Gitea with the Artix dark blue theme" /></a>
</p>

## Processing Sass

Install `dart-sass` and run `sass ./theme-arc-blue.scss ./theme-arc-blue.css`
